
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var http       = require('http');
var url        = require('url');
var morgan     = require('morgan');
var mongo      = require('mongodb');
var mongoose   = require('mongoose');
var cors       = require('cors');

var APIRouter  = require('./server/apirouter.js');

mongoose.connect('localhost:27017/appdb');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('combined'));
app.use(cors({
  'allowedHeaders': ['token', 'Content-Type'],
  'origin': '*',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false
}));

var port = process.env.PORT || 4000;
var router = express.Router();

router.get('/', function(req, res) {
  res.json({ message: 'hooray! welcome to our api!' });
});

app.use('/api', router);

router.route('/setState').post(function(req, res) {
  APIRouter.setState(req, res);
});

router.route('/getState').post(function(req, res) {
  APIRouter.getState(req, res);
});

router.route('/trackVideo').post(function(req, res) {
  APIRouter.trackVideo(req, res);
});

app.listen(port);
console.log('Magic happens on port ' + port);