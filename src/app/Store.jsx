
'use strict';

import * as HelperFn from './Helper.jsx';
import { API } from './constants.jsx';

const STORAGE_ID = 'vodKeys';
const USER_TOKEN = 'usertoken';
const STATUS_FAILURE = "failure";

export default class Store {
  constructor() {
    this.list = [];
    this.keys = {};
  }

  fetch() {
    return new Promise((resolve, reject) => {
      HelperFn.ajax({
        type: 'GET',
        url: API.movies,
        success: (res) => {
          resolve(res);
        },
        error: (res) => {
          reject(res);
        }
      });
    });
  }

  getFromServer() {
    return new Promise((resolve, reject) => {
      HelperFn.ajax({
        type: 'POST',
        url: API.getState,
        headers: [['token', localStorage.getItem(USER_TOKEN) || ""], ['Content-Type', 'application/json']],
        success: (res) => {
          res = JSON.parse(res);

          if (res.status === STATUS_FAILURE) {
            localStorage.setItem(USER_TOKEN, res.token);
          }
          
          resolve(res);
        },
        error: (res) => {

        }
      });
    });
  }

  saveToServer(data) {
    return new Promise((resolve, reject) => {
      HelperFn.ajax({
        type: 'POST',
        url: API.setState,
        data: JSON.stringify({state: data}), 
        headers: [['token', localStorage.getItem(USER_TOKEN) || ""], ['Content-Type', 'application/json']],
        success: (res) => {
          console.log(res);
        },
        error: (res) => {

        }
      });
    });
  }

  save() {
    let data = JSON.stringify(this.keys);
    localStorage.setItem(STORAGE_ID, data);
    this.saveToServer(data);
  }

  incrementCount(id) {
    this.keys[id]++;
    this.save();
  }

  createKeys() {
    try {
      this.keys = JSON.parse(localStorage[STORAGE_ID]);
    } catch(e) {
      this.keys = undefined;
    }

    if (!this.keys) {
      this.keys = {};
      this.list.forEach(item => {
        this.keys[item.id] = 0;
      });
    }
  }

  start() {
    return new Promise((resolve, reject) => {
      this.fetch().then(responseData => {
        try {
          responseData = JSON.parse(responseData);
        } catch(e) {
          responseData = {};
        }
        
        this.list = responseData.entries;
        this.createKeys();

        this.getFromServer().then((res) => {
          try {
            res.state = JSON.parse(res.state);  
          } catch(e) {
            return;
          }

          this.keys = Object.assign({}, this.keys, res.state);
        })
        resolve(this.list);
      });
    });
  }
}