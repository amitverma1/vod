'use strict';

import React from 'react';
import {render} from 'react-dom';

import AppContainer from './AppContainer.jsx';

class App extends React.Component {
	constructor(props) {
		super(props);
	}
	
  render () {
    return <AppContainer />;
  }
}


render(<App/>, document.getElementById('app'));
