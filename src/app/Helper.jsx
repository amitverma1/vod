'use strict';

export function throttle(fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  var last,
      deferTimer;
  return function () {
    var context = scope || this;

    var now = +new Date,
        args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
};

export function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

export function getDimensions() {
  return {
    width: window.innerWidth,
    height: window.innerHeight
  }
};

export function ajax(obj) {
  var ajaxSuccess = function(xhr, callback) {
		if (callback && typeof callback === 'function')
			callback(xhr.responseText);
	};

	var ajaxError = function(xhr, callback, errorMsg) {
		if (callback && typeof callback === 'function')
			callback(xhr);
		if (errorMsg)
			platformBridge.showToast(errorMsg);
	};

  var fire = function(obj, conn){
		var url = obj.url,
			headers = obj.headers,
			data = obj.data,
			errorMsg = obj.errorMessage,
			callbackSucess = obj.success,
			callbackFailure = obj.error,
			type = obj.type.toUpperCase();

		var xhr = new XMLHttpRequest();
		var xmlHttpTimeout;

		var ajaxTimeout = function(){
			xhr.abort();
		};

		if (xhr) {
			xhr.onreadystatechange = function() {
				if (4 == xhr.readyState && 200 == xhr.status) {
					clearTimeout(xmlHttpTimeout); 
					ajaxSuccess(xhr, callbackSucess);
				}
				if (4 == xhr.readyState && 200 != xhr.status) {
					clearTimeout(xmlHttpTimeout); 
					ajaxError(xhr, callbackFailure, errorMsg);
				}
			};

			var datatype = Object.prototype.toString.call(data);
			if (datatype === '[object Object]') {
        data = JSON.stringify(data);
      }

			xhr.open(type, url, true);
			
      if (headers) {
				for (var i = 0; i < headers.length; i++) {
					xhr.setRequestHeader(headers[i][0], headers[i][1]);
				}
			}

			xhr.send(data);
			
      if (obj.timeout) {
        xmlHttpTimeout = setTimeout(ajaxTimeout, obj.timeout);
      }
		}
	}

	return fire(obj);
};