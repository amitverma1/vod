'use strict';

import { ENV } from './config/constants.jsx';

class Cache {
  constructor() {

  }

  getFromCache(env, key) {
    if (env === ENV.DEV) {
      return this.getFromLocalStorage(key)
    } else {
      return this.getFromDeviceCache(key);
    }
  }

  saveToCache(env, key, value) {
    if (env === ENV.DEV) {
      this.saveToLocalStorage(key, value);
    } else {
      this.saveToDeviceCache(key, value);
    }
  }

  getFromDeviceCache(key) {
    return new Promise((resolve, reject) => {
      try {
        platformSdk.nativeReq({
          fn: 'getFromCache',
          ctx: this,
          data: key,
          success: function(response) {
            if (response !== "") {
              response = decodeURIComponent(response);
              try {
                var response = JSON.parse(response);
              } catch (e) {}
            }

            resolve(response);
          }
        });  
      } catch(e) {
        resolve();
      }
    });
  }

  getFromLocalStorage(key) {
    return new Promise((resolve, reject) => {
      var response = localStorage[key];
      try {
        response = JSON.parse(response);
      } catch(e) {}

      resolve(response);
    });
  }

  saveToLocalStorage(key, value) {
    localStorage[key] = value;
  }

  saveToDeviceCache(key, value) {
    PlatformBridge.putInCache(key, value);
  }
}

export default new Cache();