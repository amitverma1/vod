'use strict';

import React from 'react';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showCheck: this.props.historyStatus
    };
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.historyStatus !== this.props.historyStatus) {
      this.setState({
        showCheck: nextProps.historyStatus
      });
    }
  }

  render() {
    let toggleClass = this.state.showCheck ? '' : 'hide';
    return (
      <div className={'header'}>
        <div className={'logo'}>vod</div>
        <button ref={'btn'} className={'btn showHistory'} onClick={this._handleClick.bind(this)}>
          <i className={"fa fa-check " + toggleClass} aria-hidden="true"></i>
          Show History
        </button>
      </div>
    );
  }

  _handleClick() {
    this.props.toggleHistory();
  }
}
