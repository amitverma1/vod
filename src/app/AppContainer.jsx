'use strict';

import React from 'react';
import Store from './Store.jsx';
import Header from './Header.jsx';
import Carousel from './Carousel.jsx';
import FileUploader from './FileUploader.jsx';
import VideoPlayer from './VideoPlayer.jsx';

export default class AppContainer extends React.Component {
  constructor(props) {
    super(props);
    this.store = new Store();
    this.state = {
      movieList: [],
      start: false,
      showPlayer: false,
      showHistory: false,
      selectedVideo: null
    };

    this.componentWillBind();
    this.initializeStore();
  }

  componentWillBind() {
    this.launchPlayer = this.launchPlayer.bind(this);
    this.closePlayer = this.closePlayer.bind(this);
    this.incrementPlayCount = this.incrementPlayCount.bind(this);
    this.toggleHistory = this.toggleHistory.bind(this);
    this.playFile = this.playFile.bind(this);
  }

  renderAppContainer() {
    return (
      <div className={'appContainer'}>
        <Header toggleHistory={this.toggleHistory} historyStatus={this.state.showHistory} />
        <Carousel list={this.state.movieList} keylist={this.store.keys} historyStatus={this.state.showHistory} launchPlayer={this.launchPlayer} />
        <VideoPlayer showPlayer={this.state.showPlayer} data={this.state.selectedVideo} closePlayer={this.closePlayer} incrementPlayCount={this.incrementPlayCount} />
        <FileUploader playFile={this.playFile}/>
      </div>
    );
  }

  render() {
    if (this.state.start) return this.renderAppContainer();
    else return null;
  }

  initializeStore() {
    this.store.start().then(responseData => {
      this.setState({
        movieList: responseData,
        start: true
      });
    });
  }

  incrementPlayCount(id) {
    this.store.incrementCount(id);
  }

  launchPlayer(data) {
    console.log(data);
    this.setState({
      showPlayer: true,
      selectedVideo: data
    })
  }

  closePlayer() {
    this.setState({
      showPlayer: false,
      selectedVideo: null
    });
  }

  playFile(file) {
    this.launchPlayer(file);
  }

  toggleHistory() {
    let showHistory = !this.state.showHistory;
    
    this.setState({
      showHistory
    });
  }
}
