'use strict';

import React from 'react';
import VideoThumb from './VideoThumb.jsx';

export default class Carousel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      history: this.props.historyStatus
    };
  }

  componentDidMount() {
    this.initializeSwiper();
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.historyStatus !== this.props.historyStatus) {
      this.setState({
        history: nextProps.historyStatus
      }, () => {
        this.swiper.update();
      })
    }
  }

  render() {
    return (
      <div ref={'swiper'} className={'swiper-container'}>
        <div className={'swiper-wrapper'}>
          {this.props.list.map((movie, index) => {
            if (this.state.history) {
              if (this.props.keylist[movie.id]) {
                return <VideoThumb key={index} data={movie} launchPlayer={this.props.launchPlayer} />  
              } else return null;
            } else {
              return <VideoThumb key={index} data={movie} launchPlayer={this.props.launchPlayer} />
            }
          })}
        </div>
        <div className={"btn-next fa fa-chevron-right"}></div>
        <div className={"btn-prev fa fa-chevron-left"}></div>
      </div>
    );
  }

  initializeSwiper() {
    let container = this.refs['swiper'];
    this.swiper = new Swiper(container, {
      pagination: '.swiper-pagination',
      slidesPerView: 4,
      paginationClickable: true,
      spaceBetween: 2,
      keyboardControl: true,
      nextButton: '.fa-chevron-right',
      prevButton: '.fa-chevron-left',
      speed: 400,
      breakpoints: {
        320: {
          slidesPerView: 1,
        },
        640: {
          slidesPerView: 2,
        },
        920: {
          slidesPerView: 3,
        }
      }
    });   
  }
}
