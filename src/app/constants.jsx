
'use strict';

export const Meta = {
  version: '0.0.1',
};

export const API = {
  movies: 'https://demo2697834.mockable.io/movies',
  setState: 'http://localhost:4000/api/setState',
  getState: 'http://localhost:4000/api/getState',
  trackVideo: 'http://localhost:4000/api/trackVideo',
}