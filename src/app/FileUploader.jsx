
'use strict';

import React from 'react';
import Dropzone from 'react-dropzone';
import request from 'superagent';

import * as HelperFn from './Helper.jsx';
import { API } from './constants.jsx';

export default class FileUploader extends React.Component {
  constructor(props) {
    super(props);

    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  componentDidMount() {

  }

  render() {
    return (
      <div className={'fileUploadWrapper'}>
        <Dropzone
          multiple={false}
          onDrop={this.handleDrop.bind(this)}>
          <p>Drop a video or click to select a file to upload. Only Video files allowed. </p>
        </Dropzone>
      </div>
    );
  }

  handleButtonClick() {
    this.refs && this.refs.fileUpload.click();
  }

  handleFileTrigger(files) {
    console.log(files);
  }

  handleDrop(files) {
    let filedata = {};
    files.forEach(file => {
      var reader = new FileReader();
      reader.onload = ((vid) => {
        return (e) => {
          filedata.contents[0].url = e.target.result; 
          this.props.playFile(filedata);
        }
      })();

      console.log(file);

      if (file.type.split('/')[0] !== "video") {
        alert('Invalid file type');
        return;
      }

      filedata.title = file.name;
      filedata.type = "movie";
      filedata.contents = [{
        duration: 0,
        format: file.type.split('/')[1],
        geoLock: false,
        height: 360,
        width: 640,
        id: 'desktop_video_' + file.name,
      }];
      
      reader.readAsDataURL(file);
    })
  }
}