
'use strict';

import React from 'react';
import * as HelperFn from './Helper.jsx';
import videojs from 'video.js';

import { API } from './constants.jsx';

const USER_TOKEN = 'usertoken';

export default class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.logging = {};
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.showPlayer && this.props.data) {
      let video = this.props.data.contents[0];
      this.player = videojs(this.videoNode, {
        autoPlay: true,
        controls: true,
        sources: [{
          type: 'video/' + video.format,
          src: video.url
        }]
      }, () => {
        this.player.play();
      });

      this.logging[this.props.data.id] = {};

      this.player.on('timeupdate', HelperFn.throttle(this.tracker.bind(this), 1000));
      this.props.incrementPlayCount(this.props.data.id);
    }
  }

  componentWillUnmount() {
    if (this.player) {
      this.player.off('timeupdate', HelperFn.throttle(this.tracker.bind(this), 1000));
      this.player.dispose();
    }
  }

  render() {
    if (this.props.showPlayer && this.props.data) {
      let video = this.props.data && this.props.data.contents[0];
      let cats = (this.props.data && this.props.data.categories && this.props.data.categories.map(item => { return item.title })) || [];

      return (
        <div className={'videoPlayerWrapper ' + (this.props.showPlayer && this.props.data ? '' : 'hide')} ref={'target'}>
          <i className={'fa fa-times closePlayer'} onClick={this._closePlayer.bind(this)} />
          <div className={'titleWrapper'}>
            <h2>{this.props.data && this.props.data.title}</h2>
            <p>{cats && cats.join(', ')}</p>
          </div>
          <video className={'video-js vjs-default-skin vjs-big-play-centered'} ref={(c) => { this.videoNode = c; }}></video>
        </div>
      );
    } else return null;
  }

  _closePlayer() {
    if (this.player) {
      this.player.pause();
    }

    this.props.closePlayer();
  }

  getPercentage() {
    return (Math.floor(this.elapsed) / Math.floor(this.duration) * 100).toFixed(2);
  }

  sendLogs(value) {
    let data = {
      id: this.props.data.id,
      elapsedTime: this.elapsed,
      duration: this.duration,
      perc: value
    };

    this.logging[this.props.data.id][value] = true
    console.log('about to send logs to server for time', value);

    HelperFn.ajax({
      type: 'POST',
      url: API.trackVideo,
      data: JSON.stringify({state: data}), 
      headers: [['token', localStorage.getItem(USER_TOKEN) || ""], ['Content-Type', 'application/json']],
      success: (res) => {}, 
      error: (res) => {}
    });
  }

  log() {
    var value = (Math.floor(this.percentage));

    switch(true) {
      case (value < 25): 

      break;

      case (value < 50):
        if (!this.logging[this.props.data.id][25]) {
          this.sendLogs(25);
        }
      break;

      case (value < 75):
        if (!this.logging[this.props.data.id][50]) {
          this.sendLogs(50);
        }
      break;

      case (value < 100):
        if (!this.logging[this.props.data.id][75]) {
          this.sendLogs(75);
        }
      break;

      case (value === 100):
        if (!this.logging[this.props.data.id][100]) {
          this.sendLogs(100);
        }
      break;
    };
  }

  tracker() {
    this.duration = this.player.duration();
    this.elapsed = this.player.currentTime();
    this.percentage = this.getPercentage();

    this.log();
    console.log(this.elapsed + '/' + this.duration + ' = ' + this.percentage);
  }
}
