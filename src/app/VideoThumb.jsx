'use strict';

import React from 'react';

export default class VideoThumb extends React.Component {
  constructor(props) {
    super(props);
    // console.log(props);
  }

  componentWillMount() {

  }

  componentWillReceiveProps(nextProps, nextState) {

  }

  render() {
    let backgroundStyle = {
      background: 'url(' + this.props.data.images[0].url + ')',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      height: '480px'
    };

    return (
      <div className={'videoThumb swiper-slide'} onClick={this._handleClick.bind(this)} style={backgroundStyle}>
        <div className={'videoThumbInfo'}>
          <h3>{this.props.data.title}</h3>
          <p>{this.props.data.description}</p>
        </div>
      </div>
    );
  }

  _handleClick(e) {
    this.props.launchPlayer(this.props.data);
  }
}
