
var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
  userId: String,
  data: String
});

module.exports = mongoose.model('User', userSchema);