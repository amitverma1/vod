var url = require('url');

var jwt = require('jsonwebtoken');
var appsecret = "fancyappsecret";

var mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectID;
var Promise = require('promise');

var zlib = require("zlib");
var path = require('path');
var fs = require('fs');
var mkdirp = require('mkdirp');
var getDirName = require('path').dirname;

var User = require('./models/user.js');
var APIRouter = function () {};

APIRouter.prototype = {
  auth: function (token) {
    return new Promise(function(resolve, reject) {
      if (token === "" || !token) resolve(null);
      jwt.verify(token, appsecret, function(err, decoded) {			
        if (err) return null;
        else {
          console.log(decoded);
          resolve(decoded);
        }
      });
    })
  },

  getNewToken: function (user) {
    return jwt.sign({
      data: user,
    }, appsecret, { expiresIn: 365 * 7 * 1440 * 60 });
  },

  getUserFromDB: function (user) {
    return new Promise(function(resolve, reject) {
      User.findOne({
        userId: user
      }, function(err, user) {
        if (err) resolve(null);
        resolve(user);
      });
    });
  },

  createNewUser: function (req) {
    user = 'newuser_' + req.ip + '_' + new Date().getTime();
    return {
      user: user,
      token: this.getNewToken(user),
    };
  },

  getState: function(req, res) {
    var that = this;
    this.auth(req.headers.token).then(function(user){
      if (user) {
        that.getUserFromDB(user.data).then(function(db_anchor) {
          res.send({
            status: 'success',
            state: db_anchor && db_anchor.data
          });
        });
      } else {
        var newUserToken = that.createNewUser(req);
        res.send({
          status: 'failure',
          token: newUserToken.token
        });
      }
    }); 
  },

  setState: function(req, res) {
    var that = this;
    var state = req.body.state;
    var db_anchor;

    this.auth(req.headers.token).then(function(user) {
      if (!user) {
        var newUser = this.createNewUser(req);
        user = {
          data: newUser.user
        };
      }

      that.getUserFromDB(user.data).then(function(db_anchor) {
        if (!db_anchor) {
          db_anchor = new User();
          db_anchor.userId = user.data;
        }

        db_anchor.data = "" + state;
        db_anchor.save();

        res.send({
          status: 'success',
        });
      });
    });
  },

  log: function (data) {
    var dir = path.resolve(__dirname);
    var pathname = dir + "/dump/log.txt";
    mkdirp(getDirName(pathname), function (err) {
      if (err) console.log(err);
      fs.writeFile(pathname, '\n' + JSON.stringify(data), {'flag':'a'}, function(err) {
        if (err) {
          return console.log(err);
        }
      });
    });
  },

  trackVideo: function(req, res) {
    var data = req.body.state;
    this.log(data);

    // code to do whatever we want with this data.

    res.send(); 
  }
};

module.exports = new APIRouter();