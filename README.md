VOD Application Code

## To install:

Extract the contents to a directory.   
  
Run ```npm install```.   
Run ```npm run dev to build```.   

  
Run Server: 
    
Go to app directory.    
At the console, type ```node src/server.js```  

  
Run webapp:  
   
Go to app directory.    
Run ```src/index.html``` in browser.